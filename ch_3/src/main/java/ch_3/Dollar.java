package ch_3;

public class Dollar {
	public Integer amount;

	public Dollar(int i) {
		amount = i;
	}

	Dollar times(int multiplier) {
		return new Dollar(amount * multiplier);
	}

	@Override
	public boolean equals(Object object) {
		Dollar dollar = (Dollar) object;
		if (amount == dollar.amount)
			return true;
		else
			return false;
	}

}
