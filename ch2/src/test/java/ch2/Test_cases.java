package ch2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class Test_cases {

	void test() {
		fail("Not yet implemented");
	}

	@Test
	public void testMultiplication() {
		Dollar five = new Dollar(5);
		Dollar product = five.times(2);
		assertEquals(10, product.amount);
		product = five.times(3);
		assertEquals(15, product.amount);
	}

	/*
	 * public void testMultiplication() { Dollar five= new Dollar(5); five.times(2);
	 * assertEquals(10, five.amount); five.times(3); assertEquals(15, five.amount);
	 * }
	 */

}
